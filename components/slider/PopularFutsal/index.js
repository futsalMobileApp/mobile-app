import React, {Component} from 'react';
import {
  StyleSheet, 
  Text, 
  View,
  ScrollView,
  Image
} from 'react-native';

import Icon from "react-native-vector-icons/dist/FontAwesome";
class PopularFutsal extends Component{
  render() {
    return (
        <View style={{marginTop:35}}>
            <ScrollView scrollEventThrottle={16}>
                    <Text style={{
                        fontWeight:'bold',
                        fontSize:25
                    }}>Popular Futsal Field</Text>
            
                    <View style={{height:160, paddingTop:10}}>
                        <ScrollView
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}>
                            
                            <View style={{paddingLeft:20,height:200,width:200,borderWidth:0.5,borderColor:'white'}}>
                                <View style={{flex:2}}>
                                    <Image
                                        style={{flex:1, width:null,height:null,resizeMode:'cover',borderRadius:10}} 
                                        source={{uri: 'http://www.staradmiral.com/wp-content/uploads/2017/01/Empat-Macam-Lapangan-Futsal.jpg'}}/>
                                </View>
                                <View style={{flex:1,paddingLeft:10}}>
                                    <Text>Futsal</Text>
                                </View>
                            </View>
                            <View style={{paddingLeft:20,height:200,width:200,borderWidth:0.5,borderColor:'white'}}>
                                <View style={{flex:2}}>
                                    <Image
                                        style={{flex:1, width:null,height:null,resizeMode:'cover',borderRadius:10}} 
                                        source={{uri: 'http://www.staradmiral.com/wp-content/uploads/2017/01/Empat-Macam-Lapangan-Futsal.jpg'}}/>
                                </View>
                                <View style={{flex:1,paddingLeft:10}}>
                                    <Text>Futsal</Text>
                                </View>
                            </View>
                            <View style={{paddingLeft:20,height:200,width:200,borderWidth:0.5,borderColor:'white'}}>
                                <View style={{flex:2}}>
                                    <Image
                                        style={{flex:1, width:null,height:null,resizeMode:'cover',borderRadius:10}} 
                                        source={{uri: 'http://www.staradmiral.com/wp-content/uploads/2017/01/Empat-Macam-Lapangan-Futsal.jpg'}}/>
                                </View>
                                <View style={{flex:1,paddingLeft:10}}>
                                    <Text>Futsal</Text>
                                </View>
                            </View>

                            <View style={{paddingLeft:20,height:200,width:200,borderWidth:0.5,borderColor:'white'}}>
                                <View style={{flex:2}}>
                                    <Image
                                        style={{flex:1, width:null,height:null,resizeMode:'cover',borderRadius:10}} 
                                        source={{uri: 'http://www.staradmiral.com/wp-content/uploads/2017/01/Empat-Macam-Lapangan-Futsal.jpg'}}/>
                                </View>
                                <View style={{flex:1,paddingLeft:10}}>
                                    <Text>Futsal</Text>
                                </View>
                            </View>
                            
                        </ScrollView>
                    </View>
            </ScrollView>
        </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
export default PopularFutsal
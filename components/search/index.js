import React, {Component} from 'react';
import {
  StyleSheet, 
  Text, 
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';


import { withNavigation } from 'react-navigation';
import Icon from "react-native-vector-icons/dist/FontAwesome";
import DateTimePicker from 'react-native-modal-datetime-picker';

class Search extends Component{
    constructor(props){
        super(props);
        this.state = {
            key:'search'
        }
    }
    state={
        isDateTimePickerVisible: false,
        isTmePicker:false
    }

    //Date Time Picker 
    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });
    _handleDatePicked = (date) => {
        console.log('A date has been picked: ', date);
        this._hideDateTimePicker();
    };

    //TimePicker
    _showTimePicker = () => this.setState({ isTmePicker: true });
    _hideTimePicker = () => this.setState({ isTmePicker: false });
    _handleTimePicker = (time) => {
        console.log('A date has been picked: ', time);
        this._hideTimePicker();
    }

    render() {
    return (
        <View style={{marginTop:20}}>
            <TextInput
                onChangeText={(text) => this.setState({key:text})}
                // value={this.state.key}
                placeholder="location.."
                placeholderTextColor="grey"
                style={{
                    paddingLeft:25,
                    height:50,
                    fontSize:18,
                    fontWeight:'100',
                    backgroundColor: '#F3F3F3',
                    borderRadius:50
            }}/>
            <View style={{flexDirection:'row', marginTop:10, flexWrap: "wrap"}}>
                <TouchableOpacity 
                        style={{margin:3,width: 130, height: 70, backgroundColor: 'white', paddingTop:15, paddingLeft:10, borderRadius:10,borderWidth:2, borderColor:'#ADE7FE'}}
                        onPress={this._showDateTimePicker}>
                    <Text style={{fontWeight:'bold'}}>12 Des 2018</Text>
                    <Text>Starting</Text>
                    <DateTimePicker
                        isVisible={this.state.isDateTimePickerVisible}
                        onConfirm={this._handleDatePicked}
                        onCancel={this._hideDateTimePicker}
                        // mode={'time'}
                        // datePickerModeAndroid={'spinner'}
                        />
                </TouchableOpacity>
                <TouchableOpacity 
                    style={{margin:3,width: 80, height: 70, backgroundColor: 'white', paddingTop:15, paddingLeft:10, borderRadius:10,borderWidth:2, borderColor:'#ADE7FE'}}
                    onPress={this._showTimePicker}>
                    
                    <Text style={{fontWeight:'bold'}}>Start</Text>
                    <Text>16:10</Text>
                    
                    <DateTimePicker
                        isVisible={this.state.isTmePicker}
                        onConfirm={this._handleTimePicker}
                        onCancel={this._hideTimePicker}
                        mode={'time'}
                        datePickerModeAndroid={'spinner'}
                        />
                </TouchableOpacity>
                <TouchableOpacity style={{margin:3,width: 80, height: 70, backgroundColor: 'white', paddingTop:15, paddingLeft:10, borderRadius:10,borderWidth:2, borderColor:'#ADE7FE'}}>
                    <Text style={{fontWeight:'bold'}}>Time</Text>
                    <Text>1 hour</Text>
                </TouchableOpacity>
                
            </View>
            <TouchableOpacity 
                style={{margin:3,width: 80, height: 50, backgroundColor: '#ADE7FE', paddingTop:15, paddingLeft:10, borderRadius:10}}
                onPress={() => {
                    // alert(this.state.key)
                    this.props.navigation.navigate('Search',{
                        key: this.state.key
                    })
                }}
                // onPress={()=>{
                //     this.props.navigation.navigate('Search')
                // }}
                >
                <Text>Search</Text>
            </TouchableOpacity>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
export default withNavigation(Search)
import Axios from 'axios';
import { API_URL } from '../config/index'

const FETCH = Axios.create({
    baseURL: API_URL,
    timeout: 10000
})

export default FETCH
import * as firebase from 'firebase';

// Initialize Firebase
const config = {
    apiKey: "AIzaSyDbGxf-udBI0YRG7Lk0iwbPxlHxKn2GCIM",
    authDomain: "react-native-95c7f.firebaseapp.com",
    databaseURL: "https://react-native-95c7f.firebaseio.com",
    projectId: "react-native-95c7f",
    storageBucket: "react-native-95c7f.appspot.com",
    messagingSenderId: "527061212598"
};

export const FirebaseApps = firebase.initializeApp(config);

export const database = FirebaseApps.database();
export const auth = FirebaseApps.auth();
export const storage = FirebaseApps.storage();

export const signIn = (email, password) => {
    auth.signInWithEmailAndPassword(email,password).catch((err) => {
        alert(err)
    })
}
import React, {Component} from 'react';
import {
  Platform, 
  StyleSheet, 
  Text, 
  View, 
  TouchableOpacity, 
  ImageBackground, 
  Image} from 'react-native';

import Icon from "react-native-vector-icons/dist/FontAwesome5";
import IconAntDesign from 'react-native-vector-icons/dist/AntDesign';
import {FirebaseApps} from '../../app/config/firebase'
import button_style from '../../static/button-profile.json'

export default class App extends Component{
  static navigationOptions = {
    header: null,
  };

  componentDidMount(){
    var user = FirebaseApps.auth().currentUser;
    console.ignoredYellowBox = ['Warning: Timer'];

    if (user) {
      alert('Success sign in')
    } else {
      alert('please sign in before booking from this application')
      this.props.navigation.navigate('Login')
    }
  }

  render() {
    const { navigation }= this.props
    const uid = navigation.getParam('uid','CEO  |  Microsoft  |  America')
    const email = navigation.getParam('email','Bill Gates')
    return (
      <View style={styles.container}>
        <ImageBackground
            source={{
              uri: 'https://digitalsynopsis.com/wp-content/uploads/2017/02/beautiful-color-gradients-backgrounds-047-fly-high.png'
            }} 
            style={{ padding:25, height:330, resizeMode:'contain'}}
        >
          
          <Image 
              source={{
                uri: 'https://pbs.twimg.com/profile_images/988775660163252226/XpgonN0X_400x400.jpg'
              }} 
              style={{marginTop:40,borderRadius:60, borderWidth:1, padding:25, height:120,width:120, alignSelf:'center'}}
          />
          <View style={{alignSelf:'center', paddingTop: 10}}>
            <Text style={{textAlign:'center', color:'white', fontSize:20}}>{email}</Text>
            <Text style={{textAlign:'center', color:'white', fontSize:15}}>CEO  |  Microsoft  |  America</Text>
          </View>
          
          <View style={{flex:1, flexDirection:'row',alignSelf:'center', marginTop:10}}>
            {button_style.map((prop, key) => {
              return (
                <TouchableOpacity 
                  key={key}
                  onPress={()=>{this.props.navigation.navigate('Profile')}} 
                  style={{padding:10}}>
                  <Icon
                    name={prop.icon}
                    color={prop.color}
                    size={prop.size}
                    />
                </TouchableOpacity>
              )
            })}
          </View>
          <TouchableOpacity style={styles.buttonProfile}>
            <Text style={{color:'#97DDF9', fontWeight:'bold',textAlign: 'center', fontFamily:'sans-serif', fontSize: 15}}>Profile</Text>
          </TouchableOpacity>
        </ImageBackground>
        
        <View style={{alignSelf:'center',marginTop:30,flexDirection:'row',flexWrap:'wrap', marginRight:30, marginLeft:30}}>
            <TouchableOpacity 
              style={{height:80, padding: 10, marginTop: 15}}
              onPress={() =>{
                // this.props.navigation.navigate('Login')
                alert('Coming soon')
              }}>
              <View 
                style={{padding:10}}>
                <Icon
                  name={"wallet"}
                  color={'#64CBF4'}
                  size={50}
                  />
              </View>
              <Text style={{textAlign:'center'}}>Wallet</Text>
            </TouchableOpacity>

            <TouchableOpacity 
              style={{height:80, padding: 10, marginTop: 15}}
              onPress={() =>{
                // this.props.navigation.navigate('Login')
                alert('Coming soon')
              }}>
              <View 
                style={{padding:10}}>
                <Icon
                  name={"history"}
                  color={'#64CBF4'}
                  size={50}
                  />
              </View>
              <Text style={{textAlign:'center'}}>History</Text>
            </TouchableOpacity>

            <TouchableOpacity 
              style={{height:80, padding: 10, marginTop: 15}}
              onPress={() =>{
                // this.props.navigation.navigate('Login')
                alert('Coming soon')
              }}>
              <View 
                style={{padding:10}}>
                <Icon
                  name={"credit-card"}
                  color={'#64CBF4'}
                  size={50}
                  />
              </View>
              <Text style={{textAlign:'center'}}>Credit Card</Text>
            </TouchableOpacity>

            <TouchableOpacity 
              style={{height:80, padding: 10, marginTop: 15}}
              onPress={() =>{
                FirebaseApps.auth().signOut()
                this.props.navigation.navigate('Login')
              }}>
              <View 
                style={{padding:10}}>
                <IconAntDesign
                  name={"logout"}
                  color={'#64CBF4'}
                  size={50}
                  />
              </View>
              <Text style={{textAlign:'center'}}>Logout</Text>
            </TouchableOpacity>
        </View>
        {/* <View style={{marginTop:30, flex:1, flexDirection:'row', marginRight:30, marginLeft:30}}>
          <TouchableOpacity 
            onPress={()=>{
              this.props.navigation.navigate('Login')
              alert('OK')
            }}
            style={{height:90, padding: 20, marginTop: 25}}>
            <View 
              style={{padding:10}}>
              <Icon
                name={"wallet"}
                color={'#64CBF4'}
                size={50}
                />
            </View>
            <Text style={{textAlign:'center'}}>Wallet</Text>
          </TouchableOpacity>
          <TouchableOpacity 
            onPress={()=>{
              this.props.navigation.navigate('Login')}} 
            style={{height:90, padding: 20, marginTop: 25}}>
            <View
              style={{padding:10}}>
              <Icon
                name={"history"}
                color={'#64CBF4'}
                size={50}
                />
            </View>
            <Text style={{textAlign:'center'}}>History</Text>
          </TouchableOpacity>
          
          <TouchableOpacity 
            style={{height:90, padding: 20, marginTop: 25}}
            onPress={()=>{this.props.navigation.navigate('Login')}}   >
            <View
              style={{padding:10}}>
              <IconAntDesign
                name={"logout"}
                color={'#64CBF4'}
                size={50}
                />
            </View>
            <Text style={{textAlign:'center'}}>Logout</Text>
          </TouchableOpacity>
        </View> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // padding: 25,
    // flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'white',
  },
  buttonProfile: {
    marginTop: 310, 
    alignSelf:'center', 
    position: 'absolute',
    width: 200, 
    height: 40, 
    borderWidth:1.5, 
    borderColor: '#97DDF9', 
    backgroundColor:'white', 
    paddingLeft:10, 
    borderRadius:20, 
    justifyContent:'center' 
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

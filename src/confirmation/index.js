import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, Image, TouchableOpacity} from 'react-native';

import DateTimePicker from 'react-native-modal-datetime-picker';

export default class Confirmation extends Component{
    constructor(props){
        super(props);
        this.state = {
            key:'search'
        }
    }
    state={
        isDateTimePickerVisible: false,
        isTmePicker:false
    }

    //Date Time Picker 
    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });
    _handleDatePicked = (date) => {
        console.log('A date has been picked: ', date);
        this._hideDateTimePicker();
    };

    //TimePicker
    _showTimePicker = () => this.setState({ isTmePicker: true });
    _hideTimePicker = () => this.setState({ isTmePicker: false });
    _handleTimePicker = (time) => {
        console.log('A date has been picked: ', time);
        this._hideTimePicker();
    }

    componentDidMount(){
        console.ignoredYellowBox = ['Warning: Timer'];

    }
    render() {
        const { navigation } = this.props;
        const paymentMethod = navigation.getParam('method','no-params');
        const bank = navigation.getParam('bank','no-params');
        return (
            <View style={styles.container}>
            <Text style={{textAlign:'center', fontSize:30}}>{paymentMethod}</Text>
            <Text style={{textAlign:'center', fontSize:20}}>{bank}</Text>
            <View style={{margin:20}}>
                <TextInput 
                style={{borderBottomWidth:1}}
                placeholder={'Nominal'}/>
                <TextInput 
                style={{borderBottomWidth:1}}
                placeholder={'Nomor rekening'}/>
                <TextInput 
                style={{borderBottomWidth:1}}
                placeholder={'Atas Nama'}/>
            </View>
            <TouchableOpacity 
                style={{margin:10,width: 130, height: 70, backgroundColor: 'white', paddingTop:15, paddingLeft:10, borderRadius:10,borderWidth:2, borderColor:'#ADE7FE'}}
                onPress={this._showDateTimePicker}>
                    <Text style={{fontWeight:'bold'}}>Transfer Date</Text>
                    <Text>{new Date().getDate()} {new Date().getMonth()} {new Date().getFullYear()}</Text>
                    <DateTimePicker
                        isVisible={this.state.isDateTimePickerVisible}
                        onConfirm={this._handleDatePicked}
                        onCancel={this._hideDateTimePicker}
                        // mode={'time'}
                        // datePickerModeAndroid={'spinner'}
                        />
            </TouchableOpacity>
            <TouchableOpacity
            onPress={()=> {
                alert("Thank you for your confirmation")
                this.props.navigation.navigate('Home')
            }}
              style={styles.btnCheckout}>
              <Text style={{color:'white',textAlign:'center', paddingTop:15, fontSize: 18, fontWeight:'bold'}}>Submit</Text>
            </TouchableOpacity>
            </View>
      ); 
    
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'white',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  btnCheckout: {
    alignSelf: 'center',
    borderRadius: 20 ,
    width:200, 
    height:50, 
    backgroundColor:'#64CBF4'
  },
});

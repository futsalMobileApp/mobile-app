import React, {Component} from 'react';
import 'babel-polyfill';
import {
  Platform, 
  StyleSheet, 
  Text, 
  View, 
  TouchableOpacity,
  TextInput
} from 'react-native';
import { auth , database, FirebaseApps } from '../../app/config/firebase'
import Icon from "react-native-vector-icons/dist/AntDesign";
export default class App extends Component{
  static navigationOptions = {
    header: null,
  };

  constructor(props){
    super(props)
    this.state={
      email: 'default',
      password: 123    
    }
  }
  componentDidMount(){
  
  }

  __handle = () =>{
    FirebaseApps.auth().signInWithEmailAndPassword(this.state.email,this.state.password)
    .then((res) => {
      alert(JSON.stringify(res))
      this.props.navigation.navigate('Profile',{
        uid: res.user.uid,
        email: res.user.email
      })
    })
    .catch((err) => {
      alert("Wrong Email or Password")
    })
  }
  render() {
    return (
      <View style={styles.container}>
        <Text>Sign In</Text>
        <View>
          <TextInput
                onChangeText={(ctx) => {
                  this.setState({
                    email:ctx
                  })
                }}
                placeholder="Email"
                placeholderTextColor="grey"
                style={{
                    margin: 10,
                    paddingLeft:25,
                    height:50,
                    width: 250,
                    fontSize:15,
                    borderWidth:1,
                    fontWeight:'100',
                    borderColor: '#A1A1A1',
                    backgroundColor: 'white',
                    borderRadius:50
            }}/>
          <TextInput
            secureTextEntry={true}
                onChangeText={(ctx) => {
                  this.setState({
                    password:ctx
                  })
                }}
                placeholder="Password"
                placeholderTextColor="grey"
                style={{
                    margin: 10,
                    paddingLeft:25,
                    height:50,
                    width: 250,
                    fontSize:15,
                    borderWidth:1,
                    fontWeight:'100',
                    borderColor: '#A1A1A1',
                    backgroundColor: 'white',
                    borderRadius:50
            }}/>
        </View>
        <TouchableOpacity 
            style={{margin:3,width: 100, height: 50, backgroundColor: '#32A4BD', paddingLeft:10, borderRadius:20, justifyContent:'center' }}
            onPress={this.__handle}>
            <View style={{marginTop:4,flex:1, flexDirection:'row',padding:10,justifyContent:'center'}}>
              <Icon
                name="login"
                color="white"
                size={20}
                style={{marginLeft:2, marginRight:2}}/>
              <Text style={{textAlign:'center', fontSize:15, marginRight:6, color:'white'}}>Sign In</Text>
            </View>
        </TouchableOpacity>

        <TouchableOpacity 
            style={{margin:3,width: 100, height: 50, backgroundColor: '#32A4BD', paddingLeft:10, borderRadius:20, justifyContent:'center' }}
            onPress={() => {
              this.props.navigation.navigate('Register')
            }}>
            <View style={{marginTop:10,flex:1, flexDirection:'row',padding:10,justifyContent:'center'}}>
              <Text style={{textAlign:'center', fontSize:15, marginRight:6, color:'white'}}>Register</Text>
            </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

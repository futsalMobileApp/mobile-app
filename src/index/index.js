import React, {Component} from 'react';
import {
  ScrollView, 
  StyleSheet, 
  Text, 
  View,
  SafeAreaView,
  Image,
  TouchableOpacity
} from 'react-native';

import Icon from "react-native-vector-icons/dist/FontAwesome";
import Search from '../../components/search';
import { PopularFutsal } from '../../components/slider';

class Index extends Component{
    static navigationOptions = {
        header: null,
      };
      
      componentDidMount(){
        console.ignoredYellowBox = ['Warning: Timer'];

      }
  render() {
    const {navigation} = this.props;
    const email = navigation.getParam('name');
    return (
      <SafeAreaView style={{backgroundColor:'white', padding:25}}>
            <View style={{marginTop:18, padding:10}}>
                <Text style={{fontSize:22, fontWeight:'bold'}}>Hello {email}</Text>
                <TouchableOpacity 
                    
                    onPress={()=>{this.props.navigation.navigate('Profile')}} 
                    style={{position:'absolute',right:0}}>
                    <Icon
                        name="user-circle-o"
                        color="#64CBF4"
                        size={40}
                        />
                </TouchableOpacity>
            </View>
            <ScrollView showsVerticalScrollIndicator={false}>
                
                <Search navigation={this.props.navigation}/>

                <PopularFutsal/>

                <View style={{marginTop:35}}>
                <Text style={{ fontWeight:'bold', fontSize:25, flexWrap:'wrap' }}>Events</Text>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Details')} style={{height:180,marginTop:10, flexDirection:'row'}}>
                        <Image
                            style={{aspectRatio:1}} 
                            source={{uri: 'https://pbs.twimg.com/profile_images/378800000705681116/5c06bb130b778b42018ed30014772720.jpeg'}}/>
                        <View style={{marginLeft:20}}>
                            <Text>Event</Text>
                            <Text>lorem ad gipsum</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Details')} style={{height:180,marginTop:10, flexDirection:'row'}}>
                        <Image
                            style={{aspectRatio:1}} 
                            source={{uri: 'https://pbs.twimg.com/profile_images/378800000705681116/5c06bb130b778b42018ed30014772720.jpeg'}}/>
                        <View style={{marginLeft:20}}>
                            <Text>Event</Text>
                            <Text>lorem ad gipsum</Text>
                        </View>
                    </TouchableOpacity>
            </View>
            </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
export default Index
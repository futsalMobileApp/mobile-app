import React, {Component} from 'react';
import {TouchableOpacity, Button,Image,ScrollView, StyleSheet, Text, View} from 'react-native';
import Icon from "react-native-vector-icons/dist/FontAwesome5";

export default class Cart extends Component{
  static navigationOptions = {
    headerTitle: (
      <View style={{fontSize:30, marginLeft:20, flex:1, flexDirection:'row'}}>
        <Icon
          name={"shopping-cart"}
          color={'#C1CFD5'}
          size={30}/>
        <Text style={{marginLeft: 20,fontSize:25}}>My Cart</Text>
      </View>),
  };

  componentDidMount(){
    console.ignoredYellowBox = ['Warning: Timer'];

  }
  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={{margin: 30}}>
          <View style={{height:110,marginTop:15,flexDirection:'row'}}>
              <Image
                  style={{aspectRatio:1, borderRadius: 15}} 
                  source={{uri: 'https://pbs.twimg.com/profile_images/378800000705681116/5c06bb130b778b42018ed30014772720.jpeg'}}/>
              <View style={{marginLeft:20, flexWrap:'wrap', maxWidth:250}}>
                  <Text style={{fontSize: 20, paddingBottom: 10}}>Lapangan Futsal Bayangkara</Text>
                  <Text>Hours: 1</Text>
                  <Text style={{paddingTop:10,fontWeight:'bold'}}>Total: Rp, 50.000</Text>
              </View>
          </View>
          <View style={{height:110,marginTop:15,flexDirection:'row'}}>
              <Image
                  style={{aspectRatio:1, borderRadius: 15}} 
                  source={{uri: 'https://pbs.twimg.com/profile_images/378800000705681116/5c06bb130b778b42018ed30014772720.jpeg'}}/>
              <View style={{marginLeft:20, flexWrap:'wrap', maxWidth:250}}>
                  <Text style={{fontSize: 20, paddingBottom: 10}}>Lapangan Futsal Bayangkara</Text>
                  <Text>Hours: 1</Text>
                  <Text style={{paddingTop:10,fontWeight:'bold'}}>Total: Rp, 50.000</Text>
              </View>
          </View>
        </ScrollView>
        <View>
          <View style={{alignSelf:'flex-end', margin:20}}>
          <View style={{marginBottom:10}}>
            <Text style={{fontSize:15}}>Booking 1 : Rp. 50,000</Text>
            <Text style={{fontSize:15}}>Booking 2 : Rp. 50,000</Text>
          </View>
            <Text style={{fontSize:20, fontWeight:'bold'}}>Total : Rp. 100,000</Text>
          </View>
          <View style={{alignItems:'center', justifyContent:'center'}}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('Checkout')
              }} 
              style={styles.btnCheckout}>
              <Text style={{color:'white',textAlign:'center', paddingTop:15, fontSize: 18, fontWeight:'bold'}}>Checkout</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'white',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  btnCheckout: {
    borderRadius: 20 ,
    width:200, 
    height:50, 
    backgroundColor:'#64CBF4'
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

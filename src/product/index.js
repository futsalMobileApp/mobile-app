import React, {Component} from 'react';
import {Image, StyleSheet, Text, View, ScrollView, TouchableOpacity} from 'react-native';

import DateTimePicker from 'react-native-modal-datetime-picker';

export default class App extends Component{
    constructor(props){
        super(props);
        this.state = {
            key:'search'
        }
    }
    state={
        isDateTimePickerVisible: false,
        isTmePicker:false,
        isHoursPicker: false
    }

    //Date Time Picker 
    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });
    _handleDatePicked = (date) => {
        console.log('A date has been picked: ', date);
        this._hideDateTimePicker();
    };

    //TimePicker
    _showTimePicker = () => this.setState({ isTmePicker: true });
    _hideTimePicker = () => this.setState({ isTmePicker: false });
    _handleTimePicker = (time) => {
        alert('A date has been picked: '+ time);
        this._hideTimePicker();
    }

    //TimePicker
    _showHoursPicker = () => this.setState({ isHoursPicker: true });
    _hideHoursPicker = () => this.setState({ isHoursPicker: false });
    _handleHoursPicker = (hour) => {
        alert('A date has been picked: '+hour);
        this._hideTimePicker();
    }

    componentDidMount(){
        console.ignoredYellowBox = ['Warning: Timer'];
    }
    render() {
    
    const type =`Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, `
    return (
      <View style={styles.container}>
        <ScrollView style={{marginTop:10}}>
          <Text style={{
              fontWeight:'bold',
              fontSize:25,
              padding:10
          }}>Futsal Bayangkara</Text>
        
          <ScrollView scrollEventThrottle={16}>
              <View style={{height:200, paddingTop:10}}>
                  <ScrollView
                      horizontal={true}
                      showsHorizontalScrollIndicator={false}>
                      
                      <View style={styles.card}>
                          <View style={{flex:2}}>
                              <Image
                                  style={{flex:1, width:null,height:null,resizeMode:'cover'}} 
                                  source={{uri: 'http://www.staradmiral.com/wp-content/uploads/2017/01/Empat-Macam-Lapangan-Futsal.jpg'}}/>
                          </View>
                          <View style={{flex:1,paddingLeft:10}}>
                              <Text>Lapangan A</Text>
                          </View>
                      </View>
                      <View style={styles.card}>
                          <View style={{flex:2}}>
                              <Image
                                  style={{flex:1, width:null,height:null,resizeMode:'cover'}} 
                                  source={{uri: 'http://www.staradmiral.com/wp-content/uploads/2017/01/Empat-Macam-Lapangan-Futsal.jpg'}}/>
                          </View>
                          <View style={{flex:1,paddingLeft:10}}>
                              <Text>Lapangan B</Text>
                          </View>
                      </View>
                      <View style={styles.card}>
                          <View style={{flex:2}}>
                              <Image
                                  style={{flex:1, width:null,height:null,resizeMode:'cover'}} 
                                  source={{uri: 'http://www.staradmiral.com/wp-content/uploads/2017/01/Empat-Macam-Lapangan-Futsal.jpg'}}/>
                          </View>
                          <View style={{flex:1,paddingLeft:10}}>
                              <Text>Lapangan 1</Text>
                          </View>
                      </View>
                      <View style={styles.card}>
                          <View style={{flex:2}}>
                              <Image
                                  style={{flex:1, width:null,height:null,resizeMode:'cover',borderRadius:10}} 
                                  source={{uri: 'http://www.staradmiral.com/wp-content/uploads/2017/01/Empat-Macam-Lapangan-Futsal.jpg'}}/>
                          </View>
                          <View style={{flex:1,paddingLeft:10}}>
                              <Text>Lapangan 2</Text>
                          </View>
                      </View>
                      
                  </ScrollView>
              </View>
          </ScrollView>
          <View style={{margin:20, maxWidth:300, flexWrap:'wrap'}}>
            <Text style={{marginBottom:20}}>{type}</Text>
            <View style={{flexDirection:'row'}}>
                <TouchableOpacity 
                        style={{margin:3,width: 130, height: 70, backgroundColor: 'white', paddingTop:15, paddingLeft:10, borderRadius:10,borderWidth:2, borderColor:'#ADE7FE'}}
                        onPress={this._showDateTimePicker}>
                    <Text style={{fontWeight:'bold'}}>12 Des 2018</Text>
                    <Text>Starting</Text>
                    <DateTimePicker
                        isVisible={this.state.isDateTimePickerVisible}
                        onConfirm={this._handleDatePicked}
                        onCancel={this._hideDateTimePicker}
                        // mode={'time'}
                        // datePickerModeAndroid={'spinner'}
                        />
                </TouchableOpacity>
                <TouchableOpacity 
                    style={{margin:3,width: 80, height: 70, backgroundColor: 'white', paddingTop:15, paddingLeft:10, borderRadius:10,borderWidth:2, borderColor:'#ADE7FE'}}
                    onPress={this._showTimePicker}>
                    
                    <Text style={{fontWeight:'bold'}}>Start</Text>
                    <Text>16:10</Text>
                    
                    <DateTimePicker
                        isVisible={this.state.isTmePicker}
                        onConfirm={this._handleTimePicker}
                        onCancel={this._hideTimePicker}
                        mode={'time'}
                        datePickerModeAndroid={'spinner'}
                        />
                </TouchableOpacity>
                <TouchableOpacity 
                    onPress={this._showHoursPicker}
                    style={{margin:3,width: 80, height: 70, backgroundColor: 'white', paddingTop:15, paddingLeft:10, borderRadius:10,borderWidth:2, borderColor:'#ADE7FE'}}>
                    <Text style={{fontWeight:'bold'}}>End</Text>
                    <Text>17:10</Text>
                    <DateTimePicker
                        isVisible={this.state.isHoursPicker}
                        onConfirm={this._handleHoursPicker}
                        onCancel={this._hideHoursPicker}
                        mode={'time'}
                        datePickerModeAndroid={'spinner'}
                        />
                </TouchableOpacity>
            </View>
          </View>
          <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('Cart')
              }} 
              style={styles.btnCheckout}>
              <Text style={{color:'white',textAlign:'center', paddingTop:15, fontSize: 18, fontWeight:'bold'}}>Booking</Text>
            </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  card:{
    paddingLeft:20,
    height:300,
    width:300,
    borderWidth:0.5,
    borderColor:'white'
  },
  btnCheckout: {
    alignSelf:'center',
    borderRadius: 20 ,
    width:350, 
    height:50, 
    backgroundColor:'#64CBF4'
  },
});

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, Image, TouchableOpacity} from 'react-native';

export default class App extends Component{
  constructor(props){
    super(props)
    this.state={
      onConfirmation: false,
      bank:''
    }
  }
  componentDidMount(){
    console.ignoredYellowBox = ['Warning: Timer'];

  }
  render() {
    const { navigation } = this.props;
    const paymentMethod = navigation.getParam('payMethod','no-params')
    
      return(
        <View style={styles.container}>
          <Text style={{textAlign:'center', fontSize:30, marginTop:40}}>{paymentMethod}</Text>
          <View style={{margin:30}}>
            <TouchableOpacity 
              onPress={() => {
                this.props.navigation.navigate('Confirmation',{
                  method: paymentMethod,
                  bank: 'BCA'
                })
              }}
              style={{flexDirection:'row'}}>
              <Image
                  style={{width:120, height: 120, resizeMode:'contain'}} 
                  source={{uri:'https://2.bp.blogspot.com/-lOAvxqPQ23s/WgO53cUvDOI/AAAAAAAAEoo/hiWQzddn0Vcu6FTQFU3iPnfe0jBqqvZowCLcBGAs/s1600/bca.jpg'}} />
              <View style={{paddingTop:30, paddingLeft:30}}>
                <Text>Transfer BCA</Text>
                <Text>0331476778</Text>
                <Text>a.n PT. Sports Indonesia</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity 
              onPress={() => {
                this.props.navigation.navigate('Confirmation',{
                  method: paymentMethod,
                  bank: 'MANDIRI'
                })
              }}
              style={{flexDirection:'row'}}>
              <Image
                  style={{width:120, height: 120, resizeMode:'contain'}} 
                  source={{uri:'https://1.bp.blogspot.com/-QP4vt4BcT6E/WgO4eSbEKoI/AAAAAAAAEoY/QpI27S8JkN402sNJvw6QnOxX0s6Q_ub2QCLcBGAs/w1200-h630-p-k-no-nu/mandiri.jpg'}} />
              <View style={{paddingTop:30, paddingLeft:30}}>
                <Text>Transfer Mandiri</Text>
                <Text>1506-1234-4551-231</Text>
                <Text>a.n PT. Sports Indonesia</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      )
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'white',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

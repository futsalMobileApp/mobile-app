import React, {Component} from 'react';
import {TouchableOpacity, Button,Image,ScrollView, StyleSheet, Text, View} from 'react-native';
import Icon from "react-native-vector-icons/dist/FontAwesome5";

export default class Checkout extends Component{
  
    componentDidMount(){
        console.ignoredYellowBox = ['Warning: Timer'];

    }
  render() {
    return (
      <View style={styles.container}>
        <View style={{marginTop:50, marginLeft: 20}}>
            <Text style={{fontSize:30}}>Select Payment Method</Text>
            <Text style={{maxWidth:300,paddingTop:20, flexWrap:'wrap'}}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text</Text>
            <ScrollView style={{marginTop:40}} scrollEventThrottle={16}>
                <Text style={{
                    // fontWeight:'bold',
                    fontSize:25
                }}>Cards</Text>
        
                <View style={{height:300, paddingTop:10}}>
                    <ScrollView
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}>
                        <TouchableOpacity
                            onPress={() => {
                                this.props.navigation.navigate('Payment',{
                                    payMethod: 'Bank Transfer'
                                })
                            }} 
                            style={{margin:20,height:200,width:300,borderWidth:1.5,borderRadius:20,borderColor:'#68C7EF'}}>
                            <View style={{flex:1}}>
                                <Image
                                    style={{flex:1, width:null,height:null,resizeMode:'contain',borderRadius:10}} 
                                    source={{uri: 'https://restauriraia.com/wp-content/uploads/2016/03/Bank-Transfer.png'}}/>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity 
                            onPress={() => {
                                alert('Still on Process')
                            }}
                            style={{margin:20,height:200,width:300,borderWidth:1.5,borderRadius:20,borderColor:'#68C7EF'}}>
                            <View style={{flex:1}}>
                                <Image
                                    style={{flex:1, width:null,height:null,resizeMode:'contain',borderRadius:10}} 
                                    source={{uri: 'https://storage.googleapis.com/sirclo-shops/sinarmegastore/files/visa_PNG14.png'}}/>
                            </View>
                        </TouchableOpacity>
                    </ScrollView>
                </View>
            </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'white',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

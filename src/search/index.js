import React, {Component} from 'react';
import {
    TextInput, 
    StyleSheet, 
    Text, 
    View, 
    Button,
    TouchableOpacity
} from 'react-native';

export default class App extends Component{
  componentDidMount(){
    console.ignoredYellowBox = ['Warning: Timer'];

  }
  render() {
    const {navigation} = this.props;
    const search = navigation.getParam('key','location') 
    return (
      <View style={styles.container}>
        <View style={{marginTop:50}}>
            <TextInput
                // placeholder={search}
                value={search}
                placeholderTextColor='white'
                style={{
                    paddingLeft:25,
                    height:50,
                    color:'white',
                    fontSize:18,
                    width: 300,
                    fontWeight:'100',
                    backgroundColor: '#929595',
                    borderRadius:50
            }}/>
        </View>
        <Text>Search</Text>
        <Text>{JSON.stringify(search)}</Text>        
        <TouchableOpacity onPress={()=>{ this.props.navigation.navigate('Login')}}><Text>Please Login Before booking</Text></TouchableOpacity>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

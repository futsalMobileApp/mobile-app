import React, {Component} from 'react';
import {
  Platform, 
  StyleSheet, 
  Text, 
  View, 
  TouchableOpacity,
  TextInput
} from 'react-native';

import Icon from "react-native-vector-icons/dist/AntDesign";
import {FirebaseApps} from '../../app/config/firebase'

export default class Register extends Component{
  constructor(props){
    super(props)
    this.state={
      email: 'default',
      password: 123    
    }
    console.ignoredYellowBox = ['Setting a timer']
  }

  

  componentDidMount(){
    console.ignoredYellowBox = ['Warning: Timer'];

  }

  __handle = () => {
    const {email, password} = this.state;
    FirebaseApps.auth().createUserWithEmailAndPassword(email, password)
    .then((res) =>{
      alert(JSON.stringify(res.uid))
      this.props.navigation.navigate('Login')
    })
    .catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // ...
    });
  }
  render() {
    return (
      <View style={styles.container}>
        <Text>Sign In</Text>
        <View>
          <TextInput
                onChangeText={(ctx) => {
                  this.setState({
                    email:ctx
                  })
                }}
                placeholder="Email"
                placeholderTextColor="grey"
                style={{
                    margin: 10,
                    paddingLeft:25,
                    height:50,
                    width: 250,
                    fontSize:15,
                    borderWidth:1,
                    fontWeight:'100',
                    borderColor: '#A1A1A1',
                    backgroundColor: 'white',
                    borderRadius:50
            }}/>
          <TextInput
                secureTextEntry={true}
                onChangeText={(ctx) => {
                  this.setState({
                    password:ctx
                  })
                }}
                placeholder="Password"
                placeholderTextColor="grey"
                style={{
                    margin: 10,
                    paddingLeft:25,
                    height:50,
                    width: 250,
                    fontSize:15,
                    borderWidth:1,
                    fontWeight:'100',
                    borderColor: '#A1A1A1',
                    backgroundColor: 'white',
                    borderRadius:50
            }}/>
        </View>
        <TouchableOpacity 
            style={{margin:3,width: 100, height: 50, backgroundColor: '#32A4BD', paddingLeft:10, borderRadius:20, justifyContent:'center' }}
            onPress={this.__handle}>
            <View style={{marginTop:4,flex:1, flexDirection:'row',padding:10,justifyContent:'center'}}>
              <Icon
                name="login"
                color="white"
                size={20}
                style={{marginLeft:2, marginRight:2}}/>
              <Text style={{textAlign:'center', fontSize:15, marginRight:6, color:'white'}}>Register</Text>
            </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

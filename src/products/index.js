import React, {Component} from 'react';
import {
    ScrollView, 
    StyleSheet, 
    Text, 
    View,
    TextInput,
    SafeAreaView,
    Button
} from 'react-native';
import Icon from "react-native-vector-icons/dist/FontAwesome";

export default class App extends Component{
    componentDidMount(){
        console.ignoredYellowBox = ['Warning: Timer'];

    }
  render() {
    return (
        <SafeAreaView style={{}}>
            <View style={{marginTop:25, padding:10}}>
                <Text style={{fontSize:22, fontStyle:'bold'}}>Hello Maxi</Text>
                <View style={{position:'absolute',right:0}}>
                    <Icon
                        name="user-circle-o"
                        color="grey"
                        size={40}
                        />
                </View>
            </View>
            <TextInput
                underlineColorAndroid="none"
                placeholder="location.."
                placeholderTextColor="grey"
                style={{
                    marginTop:20,
                    paddingLeft:20,
                    height:50,
                    fontSize:18,
                    fontWeight:'100',
                    backgroundColor: '#F3F3F3',
                    borderRadius:50
                }}/>   
            <ScrollView>
                <View style={styles.container}>
                    <View style={{width: 100, maxHeight: 100}}>
                        <Text>Product 1</Text>
                    </View>
                <View style={{width: 100, maxHeight: 100}}>
                    <Text>Product 1</Text>
                </View>
                <View style={{width: 50, height: 50, backgroundColor: 'powderblue', margin:10}} />
                <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
                <View style={{width: 50, height: 50, backgroundColor: 'steelblue'}} />
            </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection:'row',
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'white',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

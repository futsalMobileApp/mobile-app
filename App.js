/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import { createBottomTabNavigator, createAppContainer, createStackNavigator } from 'react-navigation';

import Icon from "react-native-vector-icons/dist/FontAwesome";

import Dashboard from './src/index';
import Search from './src/search';
import Login from './src/login';
import Profile from './src/profile';
import Cart from './src/cart';
import Checkout from './src/checkout';
import Product from './src/product';
import Register from './src/register';
import Payment from './src/payment'
import Confirmation from './src/confirmation';

const Profiles = createStackNavigator({
  Profile: {screen: Profile},
  Login : {screen : Login},
  Register : {
    screen: Register
  }
},
{
  initialRouteName:'Profile'
})

const Carts = createStackNavigator({
  Cart: {screen : Cart},
  Checkout: {screen: Checkout},
  Payment: {screen: Payment},
  Confirmation: {screen: Confirmation}
},
{
  initialRouteName:'Cart'
})

const HomePage = createStackNavigator({
  Home: {screen: Dashboard},
  Details : {screen : Product},
},
{
  initialRouteName:'Home'
})

const TabNavigator = createBottomTabNavigator(
  {
    Home: HomePage
      // navigationOptions: () => {
      //     tabBarLabel: "Home"
      //     tabBarIcon: ({ focused, tintColor }) => {
      //       const iconName = 'ios-home-outline'
      //       return <Icon navIconName={iconName} size={25} color='grey' />
      //     }
      // },
    ,
    Search: {
      screen: Search,
      navigationOptions: () => {
          tabBarLabel: "Home"
          tabBarIcon: ({ focused, tintColor }) => {
            const iconName = 'ios-home-outline'
            return <Icon navIconName={iconName} size={25} color='grey' />
          }
      },
    },
    Cart: Carts,
    Profile: Profiles,
  },
  {
    order: ['Home','Search', 'Cart','Profile'],
    tabBarOptions: {
      showIcon: true,
      activeTintColor: '#D4AF37',
      inactiveTintColor: 'black',
      style: {
        backgroundColor: 'white'
      }
    }
  },
  {
    initialRouteName: 'Home'
})

export default createAppContainer(TabNavigator);